
function easeInOutExpo(t, b, c, d) {
  t /= d / 2;
  if (t < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;

  return c / 2 * (-Math.pow(2, -10 * (t - 1)) + 2) + b;
}

export default class WaveDraw {
  constructor(ctx, width, height) {
    this.ctx = ctx;
    this.width = width;
    this.height = height;
    this.points = [];
    this.offsets = [];
    this.time = 0;
    this.totalTime = 20;
    setInterval(() => {
      this.draw();
    }, 50);
  }
  nextFrame(data) {
    let { points } = this;

    if (points.length > data.length) {
      points = points.slice(0, data.length);
    } else {
      const c = data.length - points.length;
      const startNow = this.height / 2;
      for (let i = 0; i < c; i += 1) {
        points.push({
          now: startNow,
          offset: 0,
          start: 0,
        });
      }
    }

    points.forEach((point, index) => {
      point.start = point.now;
      point.offset = data[index] - point.start;
    });
    this.time = 0;
  }
  draw() {
    this.time += 1;

    const {
      ctx, points, width, height, totalTime, time,
    } = this;

    if (time >= totalTime) {
      this.time = totalTime - 1;
    }

    ctx.setGlobalAlpha(0.6);

    ctx.setFillStyle('#353535');
    ctx.fillRect(0, 0, width, height);
    ctx.setStrokeStyle('#00ff00');
    ctx.setLineWidth(2);
    const space = Math.floor(this.width / points.length);
    for (let i = 0; i < points.length; i += 1) {
      const point = points[i];
      point.now = easeInOutExpo(time, point.start, point.offset, totalTime);
      if (i === 0) {
        ctx.moveTo(0, Math.round(point.now));
      }
      ctx.lineTo(space * i, Math.round(point.now));
    }

    console.log(points.map(vo => vo.now));

    ctx.stroke();
    ctx.draw(true);
  }
}
