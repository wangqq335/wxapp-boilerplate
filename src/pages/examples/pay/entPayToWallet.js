import wallet from '@caomei/wallet/lib/wxApp';
import entPayToWalletApi from './apis/entPayToWallet';

Page({
  data: {
    result: null,
    money: 0,
    productType: 1007,
  },

  inputMoney(e) {
    this.setData({
      money: parseInt(e.detail.value, 10) || 0,
    });
  },

  async submit() {
    try {
      const { money, productType } = this.data;
      if (money <= 0) {
        throw new Error(`提取的金额${money} 需要大于0`);
      }
      const { data: result, code, message } = await entPayToWalletApi(productType, money);

      // 请求错误
      if (code !== 1) {
        throw new Error(message);
      }

      this.setData({
        result,
        paying: false,
      });

      // 唤起支付
      const res = await wallet.wxpay.payIt(result);

      console.log('支付完成', res);
    } catch (err) {
      console.error(err);
      wx.showModal({
        title: err.name,
        content: err.message,
        showCancel: false,
      });
    }
  },
});
